function niceNum(val) { // https://stackoverflow.com/a/2901298
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
};

var sportovci = {
  "Čeští sportovci" : [
  ["Jakub Voráček (hokej, Philadelphia Flyers)", 322000000],
  ["Petr Čech (fotbal, Arsenal Londýn)",236000000],
  ["David Krejčí (hokej, Boston Bruins)",167000000],
  ["David Pastrňák (hokej, Boston Bruins)",153000000],
  ["Jaromír Jágr (hokej, Florida Panthers)",146500000],
  ["Karolína Plíšková (tenis)",134000000],
  ["Jan Veselý (basketbal, Fenerbahce Istanbul)",109000000],
  ["Bořek Dočkal (fotbal, Sparta/Che-nan Ťien-jie)",82500000],
  ["Vojtěch Růžička (poker)",56500000],
  ["Tomáš Rosický (fotbal, Sparta Praha)",12000000]],
  "Zahraniční sportovci" : [
  ["Cristiano Ronaldo (fotbal, Real Madrid)",2139000000],
  [" Neymar (fotbal, PSG)",2139000000],
  ["LeBron James (basketbal, Cleveland Cavaliers)",1978000000],
  ["Lionel Messi (fotbal, Barcelona)",1840000000],
  ["Roger Federer (tenis)",1472000000],
  ["Andrew Luck  (americký fotbal, Indianapolis Colts)",1150000000],
  ["Rory McIlroy (golf)",1150000000],
  ["Lewis Hamilton (formule 1, Mercedes)",1058000000],
  ["Usain Bolt (atletika)",787000000],
  ["Sidney Crosby (hokej, Pittsburgh Penguins)",354000000],
  ["Serena Williamsová (tenis)",621000000]],
  "Běžní čeští zaměstnanci" : [
  ["Jana Nováková (zaměstnankyně s průměrnou mzdou)", 352152],
  ["Jan Novák (zaměstnanec s minimální mzdou)", 132000]]
}

var sekund = 0;

var vyberSportovce = new Vue({
  el: '#kalkulacka',
  data: {
    sportovci: sportovci,
    vybranySportovec: ["Jakub Voráček (hokej, Philadelphia Flyers)", 322000000],
    zadanyPlat: "25000",
    sekund: sekund
  },
  created: function() {
  var self = this
  setTimeout(function cycle() {
    self.sekund = self.sekund+1
    setTimeout(cycle, 1000)
    }, 1000)
  },
  computed: {
    prijmeni: function () {
      return this.vybranySportovec[0].split(/\s+/)[1]
    },
    zaKolikMinut: function () {
      return this.zadanyPlat/(this.vybranySportovec[1]/525948.766)
    },
    kolikLet: function () {
      return this.vybranySportovec[1]/(this.zadanyPlat*12)
    },
    rokZacit: function () {
      var letopocet = (new Date()).getFullYear() - Math.floor(this.vybranySportovec[1]/(this.zadanyPlat*12));
      if (letopocet >= 0) {
          return letopocet;
      }
      if (letopocet < 0) {
          return Math.abs(letopocet) + " před naším letopočtem";
      }
    },
    prumerCZ: function() {
      return this.zadanyPlat/29346*100
    },
    prumerSvet: function() {
      return this.zadanyPlat/23583
    },
    martaIndex: function() {
      return this.vybranySportovec[1]/909051
    },
    vterinaSportovec: function() {
      return this.vybranySportovec[1]/31556926*this.sekund
    },
    vterinaUser: function() {
      return this.zadanyPlat*12/31556926*this.sekund
    }
  },
  methods: {
    niceNum: function(val) {
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }
  }
})
