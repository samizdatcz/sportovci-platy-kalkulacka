title: "Jak dlouho byste vydělávali na plat Jaromíra Jágra nebo Petra Čecha? Porovnejte si svůj plat se sportovními hvězdami"
perex: "Sport je celosvětově sledovaným fenoménem, i proto se v něm točí obrovské finanční prostředky. Fotbalové prostředí nedávno šokoval přestup útočníka Neymara z Barcelony do Paris Saint-Germain za 222 milionů eur, téměř šest miliard korun."
authors: ["Vojtěch Man", "Petr Kočí"]
published: "16. září 2017"
coverimg: https://www.irozhlas.cz/sites/default/files/images/03650290.jpeg
coverimg_note: "Foto <a href='#'>ČTK</a>"
styles: []
libraries: ["https://vuejs.org/js/vue.min.js"]
options: "" #wide
---
<figure class="pull-left pull-none--m img--w200 img--w100p--m">
				<p class="img img--20x27" data-text-version="Načíst obrázek">
					<a href="#" class="img__holder is-loaded is-visible" >
						<!--
							mobile 16x9
							desktop 20x27
						-->
						<noscript>
							&lt;img src="../img/illust/200x270.jpg" width="200" height="270" alt=""&gt;
						</noscript>
					<img src="https://www.irozhlas.cz/sites/default/files/styles/zpravy_rubrikovy_nahled_vyskovy/public/uploader/2017-08-04t145600z_1_170813-163952_nis.jpg?itok=k73r1oq-"></a>
				</p>
				<figcaption>
					Brazilec Neymar s novým dresem PSG | Foto: Reuters
				</figcaption>
			</figure>

Zkrátka nepřijde ani Brazilec, jeho plat má činit 45 milionů eur ročně. Výplaty od týmů či vyhrané odměny ale nejsou jedinými příjmy sportovních superhvězd. Další miliony jim vynáší sponzorské smlouvy, proto hrubé příjmy těch nejznámějších přesahují v přepočtu dvě miliardy korun ročně.

Právě [u Neymara se předpokládá](https://www.forbes.com/sites/christinasettimi/2017/08/03/neymar-set-to-eclipse-messi-and-ronaldo-and-become-the-worlds-highest-paid-soccer-player/#4ab8f5371912), že v příštích měsících minimálně vyrovná dlouhodobého vládce finančních žebříčku Cristiana Ronalda. V jednoduché tabulce datových žurnalistů Českého rozhlasu si nyní můžete porovnat svůj příjem s výdělky těch nejlépe ohodnocených sportovců.

Velké částky si sportem vydělávají i Češi. Zatímco hokejoví boháči ve světovém srovnání pokulhávají, v českém žebříčku vládnou. Jakub Voráček si za uplynulou sezonu vydělal 322 milionů korun a dobře si vedou i další. David Pastrňák po výjimečné sezoně podepsal ve 21 letech v Bostonu nový kontrakt na v přepočtu 153 milionů korun ročně. Fotbal mezi nadvládou hokejistů zastupuje Petr Čech.

<div id="kalkulacka" style="background-color:	#F0F0F0; padding:25px; padding-bottom:10px; margin-bottom:30px; border-radius:10px">
  <form>
      <label><h3><span style="display:inline-block; width:1.5em; height:1.5em; border-radius:1em; background-color:black;text-align:center;line-height:1.4em;color:white">1</span> Zadejte svůj hrubý měsíční příjem v korunách:</h3></label>
      <input v-model.number="zadanyPlat" type="number" autofocus="true" max="99999999" min="100" required="true" step="100" style="margin-top:0.3em">
      <span v-if="!zadanyPlat>0" style="color : red"> Zadejte číslo větší než 0</span>
      <span v-if="zadanyPlat>99999999" style="color : red"> Zadejte číslo menší než 100 milionů</span>
      <br><br>
      <label><h3><span style="display:inline-block; width:1.5em; height:1.5em; border-radius:1em; background-color:black;text-align:center;line-height:1.4em;color:white">2</span> S kým se chcete porovnat?</h3></label>
      <select v-model="vybranySportovec" style="margin-top:0.3em;max-width:100%">
        <optgroup v-for="(skupina, sportovec) in sportovci" v-bind:label="sportovec">
        <option v-for="sportovec in skupina" v-bind:value="sportovec">
          {{ sportovec[0] }}
      </select>
  </form>
<hr>
<ul v-if="zadanyPlat>0&&zadanyPlat<100000000">
  <li>{{ prijmeni }} má roční příjem <strong>{{ (vybranySportovec[1]/1000000).toFixed(1) }} milionů korun</strong>. Na váš měsíční plat si vydělá <strong>za {{zaKolikMinut.toFixed(1)}} minut</strong>.</li>
  <li>S vaším současným platem by vám trvalo <strong>{{ kolikLet.toFixed(0) }} let</strong>, abyste si vydělali tolik jako vybraný člověk za jediný rok.</li>
  <li>Kdybyste s doháněním tohoto platového náskoku začali <strong>v roce {{ rokZacit }}</strong>, byli byste teď už skoro v cíli.</li>
  <li>Váš měsíční příjem představuje <strong>{{ prumerCZ.toFixed(0) }} procent</strong> aktuální průměrné mzdy v České republice a <strong>{{ prumerSvet.toFixed(1) }} násobek</strong> celosvětové průměrné mzdy.</li>
<!--  <li>{{ prijmeni }} vydělává <strong>{{ martaIndex.toFixed(0) }}krát víc než Marta</strong>, jedna z nejlepších fotbalistek světa, která si jako profesionální hráčka ve Spojených státech vydělá 41 700 dolarů (910 tisíc korun) za rok.</li>-->
  <li>Od chvíle, kdy jste otevřeli tuto stránku, jste si vydělali <strong>{{ vterinaUser.toFixed(2) }} korun</strong>, {{ prijmeni}} má za stejnou dobu na kontě <strong>{{ vterinaSportovec.toFixed(2) }} korun.</strong></li>
</ul>
</div>

Ženy sportem vydělávají méně. Ve stovce nejbohatších sportovců světa je [jen jedna žena – Serena Williamsová](https://www.forbes.com/sites/kurtbadenhausen/2017/08/14/the-highest-paid-female-athletes-2017/#57d871d53d0b). Srovnávání tenisových dotací mezi oběma pohlavími je vidět i v českém žebříčku. Do české elity výrazně zasáhla Karolína Plíšková i předtím, než se stala světovou jedničkou. Dobře si vedou také další zástupkyně „bílého sportu.“

<figure class="pull-right pull-none--m img--w238 img--w100p--m">
				<p class="img img--16x9" data-text-version="Načíst obrázek">
					<a href="#" class="img__holder is-loaded is-visible">
						<!--
							mobile + desktop 16x9
						-->
						<noscript>
							&lt;img src="../img/illust/238x134.jpg" width="238" height="134" alt=""&gt;
						</noscript>
					<img src="https://www.irozhlas.cz/sites/default/files/styles/zpravy_rubrikovy_nahled/public/images/03664857.jpeg?itok=0OZKGZqd"></a>
				</p>
				<figcaption>
					Serena Williamsová | Foto: Reuters
				</figcaption>
			</figure>

České kluby platy svých hráčů až na výjimky nezveřejňují, oproti zahraničním ligám si ale Češi v domácích soutěžích vydělají zlomek. Větší roli navíc hrají bonusy za týmové či individuální výkony, například získané body či vstřelené góly.

Nejlépe placeným Čechem v domácích soutěžích má být sparťanský navrátilec z Arsenalu Tomáš Rosický. Situace se ale může v období obrovských investic, zejména do fotbalových klubů Sparty a Slavie, měnit.

Metodologie: Výpočet je pouze orientační, nebere například v úvahu délku pracovní doby. Pro výpočet jsme použili [analýzu magazínu Forbes](https://www.forbes.com/athletes/list/), který v české i mezinárodní mutaci vydal žebříčky nejlépe vydělávajících sportovců v období od července 2016 do června 2017.

V žebříčku jsme ponechali Jaromíra Jágra, přestože pro příští hokejovou sezonu stále nemá podepsanou smlouvu. Přidali jsme Davida Pastrňáka, který se podpisem nové smlouvy v Bostonu stal pátým nejlépe vydělávajícím českým sportovcem, a Neymara, jehož příjmy se podle Forbesu po přestupu do PSG vyšplhají na 93 milionů dolarů ročně, čímž zřejmě vyrovná Cristiana Ronalda.
České kluby platy svých hráčů povětšinou nezveřejňují, přesto jsme do seznamu zařadili Tomáše Rosického, jehož výdělek při dobrém zdravotním stavu se odhaduje na milion korun měsíčně.
Údaje, které do tabulky zadáte, Český rozhlas neukládá, tudíž je nemůže použít k žádným jiným účelům.

[Inspirováno kalkulačkou BBC](http://www.bbc.com/sport/41037621)
